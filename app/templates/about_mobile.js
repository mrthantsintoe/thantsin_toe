module.exports = (user) => {
    const about_mobile_html = `
        <div class="profile-body__tab-content__about">
            <div class="mobile-about-content--header">
                <h4>About</h4>
                <button class="mobile-about-content--header__editbtn">
                    <i class="icon ion-edit"></i>
                </button>
            </div>
            <div class="mobile-about-content--body">

                <p class="mobile-about-content__name">${user.Name}</p>

                <p class="mobile-about-content__website">
                    <i class="icon ion-earth"></i> 
                    <span>${user.Website}</span>
                </p>


                <p class="mobile-about-content__mobile">
                    <i class="icon ion-ios-telephone-outline"></i> 
                    <span>${user.Mobile}</span>
                </p>


                <p class="mobile-about-content__address">
                    <i class="icon ion-ios-home-outline"></i> 
                    <span>${user.Address}</span>
                </p>

            </div>
        </div>`;


    document.querySelector(".profile-body__tab-content").insertAdjacentHTML('afterbegin',about_mobile_html);

    let profileName = document.querySelector(".profile-summary__detail__photoandbio__name");
    let profileAddress = document.querySelector(".profile-summary__detail__photoandbio__address span");
    let profileMobile = document.querySelector(".profile-summary__detail__photoandbio__mobile span");

    profileName.textContent = user.Name;
    profileMobile.textContent = user.Mobile;
    profileAddress.textContent = user.Address;
}