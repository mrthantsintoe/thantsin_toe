module.exports = (user) => {
    const about_desktop_html = `
    <div class="profile-body__tab-content__about">
        <h4>About</h4>
        <div class="about-content">
            <div class="about-content__name">
                <p class="about-content__paragraph">${user.Name}</p>
                <div class="about-content__edit-btn" data-title="Name"><i class="icon ion-edit"></i></div>

            </div>
            <div class="about-content__website">
                <div class="about-content__paragraph">
                    <i class="icon ion-earth"></i> 
                    <span>${user.Website}</span>
                </div>
                <div class="about-content__edit-btn" data-title="Website"><i class="icon ion-edit"></i></div>
            </div>
            <div class="about-content__mobile">
                <div class="about-content__paragraph">
                    <i class="icon ion-ios-telephone-outline"></i> 
                    <span>${user.Mobile}</span>
                </div>
                <div class="about-content__edit-btn" data-title="Mobile"><i class="icon ion-edit"></i></div>
            </div>
            <div class="about-content__address">
                <div class="about-content__paragraph">
                    <i class="icon ion-ios-home-outline"></i> 
                    <span>${user.Address}</span>
                </div>
                <div class="about-content__edit-btn" data-title="Address"><i class="icon ion-edit"></i></div>
            </div>
        </div>
    </div>`;

    document.querySelector(".profile-body__tab-content").insertAdjacentHTML('afterbegin',about_desktop_html);

    let profileName = document.querySelector(".profile-summary__detail__photoandbio__name");
    let profileAddress = document.querySelector(".profile-summary__detail__photoandbio__address span");
    let profileMobile = document.querySelector(".profile-summary__detail__photoandbio__mobile span");

    profileName.textContent = user.Name;
    profileMobile.textContent = user.Mobile;
    profileAddress.textContent = user.Address;
}