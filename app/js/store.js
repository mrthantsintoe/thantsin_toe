let defaultStore = {
    currentUser : {
        Name : 'Jessica Parker',
        Website : 'www.seller.com',
        Mobile : '(949) 325-68594',
        Address : 'New Port Beach, CA'
    }
}
let store = JSON.parse(localStorage.getItem('store')) || defaultStore;

module.exports = store;