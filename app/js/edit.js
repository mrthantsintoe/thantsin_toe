let renderAboutUserForDesktop = require('../templates/about_desktop');
let renderAboutUserForMobile = require('../templates/about_mobile');
let store = require('./store');

module.exports = function() {

    const attachClickListenersOnEditBtns = () => {
        const editButton = document.querySelectorAll(".about-content__edit-btn");
        for (let i=0; i < editButton.length; i++) {
            editButton[i].addEventListener("click", (event) => {
                let popupTitle = event.currentTarget.getAttribute('data-title');
                togglePopup(event, {popupTitle, value : store.currentUser[popupTitle]});
            });
        };
    }

    const saveStore = (newStore) => {
        store = newStore;
        localStorage.setItem('store', JSON.stringify(newStore));
    }

    const renderDesktopUserBasedOnWidth = () => {
        if (window.innerWidth > 480) {
            renderAboutUserForDesktop(store.currentUser);
            attachClickListenersOnEditBtns();
        }
    }

    const mutateUser = (action) => {
        let newStore = {...store};
        newStore.currentUser[action.type] = action.payload;
        removePopup();
        renderDesktopUserBasedOnWidth();
        saveStore(newStore);
    }

    const renderPopup = (event, data) => {
        let popupHTML = `<div class="popup-wrapper">
                            <div class="popup-arrow"></div>
                            <div class="popup">
                                <form class="popup__edit-form">
                                    
                                    <input type="text" value="" name="${data.popupTitle}" class="popup__edit-form__edit-text" required>
                                    <label class="popup__edit-form__label">${data.popupTitle}</label>
                            
                                    <div class="form-button-group">
                                        <button type="submit" class="popup__edit-form__save-btn">Save</button>
                                        <button type="button" class="popup__edit-form__cancel-btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>`
        event.currentTarget.insertAdjacentHTML('afterend', popupHTML);
        let submitButton = document.querySelector(".popup__edit-form__save-btn");
        let cancelButton = document.querySelector(".popup__edit-form__cancel-btn");

        cancelButton.addEventListener("click", event => {
            removePopup();
        });

        submitButton.addEventListener("click", event => {
            event.preventDefault();
            let type = document.querySelector('.popup__edit-form__edit-text').getAttribute('name');
            let payload = document.querySelector('.popup__edit-form__edit-text').value;
            if (payload.length > 0) {
                document.querySelector(".profile-body__tab-content__about").remove();
                mutateUser({
                    type,
                    payload
                });
            }
        });
    }

    const removePopup = () => {
        let currentPopup = document.querySelector('.popup-wrapper'); 
        if (currentPopup) {
            currentPopup.remove();
        }
    }

    const togglePopup = (event, data) => {
        let currentPopup = document.querySelector('.popup-wrapper');
        if (currentPopup === null) {
            renderPopup(event, data);
        } else {
            removePopup();
        }
    }

    renderDesktopUserBasedOnWidth();

    // Functions For Mobile Screen Size
    const renderMobileUserBasedOnWidth = () => {
        if (window.innerWidth <= 480) {
            renderAboutUserForMobile(store.currentUser);
            attachClickListenersOnMobileEditBtn();
        }
    }

    const renderEditForm = (event, user) => {
        let firstname = user.Name.split(' ')[0];
        let lastname = user.Name.split(' ')[1];
        let mobileEditFormHTML = `
            <div class="profile-body__tab-content__about">
                <form class="mobile__edit-form">
                    <div class="mobile__edit-form__header">
                        <h4>About</h4>
                        <div class="edit-form-button-group">
                            <button type="button" class="mobile__edit-form__header__cancel_btn">Cancel</button>
                            <button type="submit" class="mobile__edit-form__header__submit_btn">Save</button>
                        </div>
                    </div>
                    <div class="mobile__edit-form__content">
                        <input type="text" value="" name="firstname" class="popup__edit-form__edit-text" required>
                        <label>First Name</label>
                        
                        <input type="text" value="" name="lastname" class="popup__edit-form__edit-text" required>
                        <label>Last Name</label>

                        <input type="text" value="" name="website" class="popup__edit-form__edit-text" required>
                        <label>Website</label>
                
                        <input type="text" value="" name="mobile" class="popup__edit-form__edit-text" required>
                        <label>Mobile</label>
                        
                        <input type="text" value="" name="address" class="popup__edit-form__edit-text" required>
                        <label>Address</label>
                    </div>
                </form>
            </div>`;
        
        document.querySelector(".profile-body__tab-content").insertAdjacentHTML('afterbegin', mobileEditFormHTML);

        let submitButton = document.querySelector(".mobile__edit-form__header__submit_btn");
        let cancelButton = document.querySelector(".mobile__edit-form__header__cancel_btn");

        cancelButton.addEventListener("click", event => {
            document.querySelector(".profile-body__tab-content__about").remove();
            renderMobileUserBasedOnWidth();
        });

        submitButton.addEventListener("click", event => {
            event.preventDefault();
            let firstname = document.querySelector('input[name="firstname"').value;
            let lastname = document.querySelector('input[name="lastname"').value;
            let Name = firstname + " " + lastname;

            let Website = document.querySelector('input[name="website"').value;
            let Mobile = document.querySelector('input[name="mobile"').value;
            let Address = document.querySelector('input[name="address"').value;

            if (Name.length > 0 && Mobile.length > 0 && Website.length > 0 && Address.length > 0) {
                mutateUser({type : 'Name', payload : Name});
                mutateUser({type : 'Website', payload : Website});
                mutateUser({type : 'Mobile', payload : Mobile});
                mutateUser({type : 'Address', payload : Address});
                document.querySelector(".profile-body__tab-content__about").remove();
                renderMobileUserBasedOnWidth();
            }
        });
    }

    const toggleEditForm = (event, user) => {
        let currentEditForm = document.querySelector('.mobile__edit-form');
        if (currentEditForm === null) {
            document.querySelector(".profile-body__tab-content__about").remove();
            renderEditForm(event, user);
        } else {
            renderMobileUserBasedOnWidth();
            attachClickListenersOnMobileEditBtn();
        }
    }

    const attachClickListenersOnMobileEditBtn = () => {
        let editFormButton = document.querySelector(".mobile-about-content--header__editbtn");
        editFormButton.addEventListener("click", (event) => {
            toggleEditForm(event, store.currentUser);
        });
    }

    renderMobileUserBasedOnWidth();

    window.onresize = function(event) {
        if (document.querySelector(".profile-body__tab-content__about")) {
            document.querySelector(".profile-body__tab-content__about").remove();
        }
        renderDesktopUserBasedOnWidth();
        renderMobileUserBasedOnWidth();
    };

};

