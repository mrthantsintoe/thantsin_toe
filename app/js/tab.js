module.exports = function() {
    const MenuItems = document.querySelectorAll('.menu-item');
    const switchTab = function(evt, targetClass) {

        let i, tabcontent, tablinks;
        evt.preventDefault();

        tabcontent = document.querySelectorAll(".profile-body__tab-content > div");
        for (let i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        for (let i=0; i < MenuItems.length; i++) {
            MenuItems[i].classList.remove('menu-item--active');
        }

        document.querySelector(`.${targetClass}`).style.display = "block";
        evt.currentTarget.classList.add('menu-item--active');
    }

    for (let i=0; i < MenuItems.length; i++) {
        let target = MenuItems[i].getAttribute('data-target');
        MenuItems[i].addEventListener('click', function(event) {
            switchTab(event, target);
        });
    }
}