module.exports = {
  entry: ["babel-polyfill",'./app/js/main.js'],
  output: {
    path: __dirname,
    filename: './app/js/dist/bundle.js'
  },  
  module: { 
    rules: [
        {
          loader: 'babel-loader',
          test: /\.js?$/,
          exclude: /(node_modules|bower_components)/
        }
      ]
    }
  };