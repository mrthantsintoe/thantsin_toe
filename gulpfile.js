const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();

gulp.task('sass', function(){
    return gulp.src('./app/sass/*.sass')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./app/css'))
      .pipe(browserSync.reload({
        stream: true
      }))
});

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'app'
    },
  })
});


gulp.task('watch', ['browserSync', 'sass'], function (){
  gulp.watch('app/sass/*.sass', ['sass']);
  gulp.watch('app/*.html', browserSync.reload); 
  gulp.watch('app/js/*.js', browserSync.reload); 
  // Other watchers
})